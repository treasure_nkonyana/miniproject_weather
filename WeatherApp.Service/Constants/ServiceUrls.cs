﻿using System;
namespace WeatherApp.Service.Constants
{
    public class ServiceUrls
    {
        public const string baseUrl = "http://api.openweathermap.org/";
        public const string data = "data/2.5/weather?";
        public const string apikey = "b674314020516236ee7d5729a724ec20";
        public const string image = "img/w/";
    }
}
