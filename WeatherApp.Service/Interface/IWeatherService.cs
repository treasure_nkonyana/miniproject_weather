﻿using System;
using System.Threading.Tasks;
using WeatherApp.Models.Request;
using WeatherApp.Models.Response;
namespace WeatherApp.Service.Interface
{
    public interface IWeatherService
    {
        Task<WeatherObject> GetWeatherInfoByGeoAsync(WeatherRequest weatherRequest);
    }
}
