﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WeatherApp.Models.Request;
using WeatherApp.Models.Response;
using WeatherApp.Service.Constants;
using WeatherApp.Service.Interface;

namespace WeatherApp.Service.Implementation
{
    public class WeatherService : IWeatherService
    {
        public async Task<WeatherObject> GetWeatherInfoByGeoAsync(WeatherRequest weatherRequest)
        {
            try
            {
                var baseUrl = ServiceUrls.baseUrl + ServiceUrls.data + $"lat={weatherRequest.lat.ToString()}&lon={weatherRequest.lon.ToString()}&appid={ServiceUrls.apikey}";

                using (var client = new WebClient())
                {
                    client.Headers.Add("content-type", "application/json");

                    var response = await client.DownloadStringTaskAsync(baseUrl);

                    return JsonConvert.DeserializeObject<WeatherObject>(response);
                }
            }
            catch(Exception)
            {
                throw;
            }
          
        }
    }
}
