﻿using System;


namespace WeatherApp.Models.Response
{
    public class Coord
    {
        public string lon { get; set; } 
        public string lat { get; set; }
    }
}
