﻿using System;
using System.Collections.Generic;

namespace WeatherApp.Models.Response
{
    public class WeatherObject
    {
        public Coord coord { get; set; }

        public List<Weather> weather { get; set; }

        public string @base { get; set; }

        public Main main { get; set; }

        public Wind wind { get; set; }

        public Clouds clouds { get; set; }

        public string dt { get; set; }

        public Sys sys { get; set; }

        public string timezone { get; set; }

        public string id { get; set; }

        public string name { get; set; }

        public string cod { get; set; }
    }
}
