﻿using System;
namespace WeatherApp.Models.Response
{
    public class Wind
    {
        public double speed { get; set; }

        public string deg { get; set; }

        public string gust { get; set; }
    }
}
