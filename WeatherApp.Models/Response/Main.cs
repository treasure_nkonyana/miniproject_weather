﻿using System;
namespace WeatherApp.Models.Response
{
    public class Main
    {
        public double temp { get; set; }

        public string pressure { get; set; }

        public double humidity { get; set; }

        public double temp_min { get; set; }

        public double temp_max { get; set; }
    }
}
