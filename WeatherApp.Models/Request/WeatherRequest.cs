﻿using System;
namespace WeatherApp.Models.Request
{
    public class WeatherRequest
    {
        public double lat { get; set; }
        public double lon { get; set; }
    }
}
