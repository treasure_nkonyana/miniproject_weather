﻿using System;
namespace WeatherApp.Constants
{
    public class StringConstants
    {
        public const string DIALOG_ERROR_TITLE = "Error";
        public const string DIALOG_OK = "OK";
    }
}
