﻿using System;
using Prism;
using Prism.Ioc;
using Prism.Unity;
using WeatherApp.Service.Implementation;
using WeatherApp.Service.Interface;
using WeatherApp.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WeatherApp
{
    public partial class App : PrismApplication
    {
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }



        protected override void OnInitialized()
        {
            InitializeComponent();
            NavigationService.NavigateAsync("Dashboard");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            #region Navigation

            containerRegistry.RegisterForNavigation<Dashboard>();

            #endregion

            #region Data binding

            containerRegistry.Register<IWeatherService, WeatherService>();

            #endregion
        }
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
