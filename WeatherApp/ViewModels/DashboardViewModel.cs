﻿using System;
using System.Threading.Tasks;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using WeatherApp.Models.Request;
using WeatherApp.Models.Response;
using WeatherApp.Service.Interface;
using Xamarin.Essentials;

namespace WeatherApp.ViewModels
{
    public class DashboardViewModel:ViewModelBase
    {

        #region variables

        private readonly IWeatherService _weatherService;


        public double MaxTemp { get; set; }
        public double MinTemp { get; set; }
        public double Temp { get; set; }
        public string Icon { get; set; }
        public string Description { get; set; }
        public double Humidity { get; set; }
        public double Speed { get; set; }
        public bool IsRefreshing { get; set; }
        public DateTime LastRefreshed { get; set; }
        public string LocationName { get; set; }
        #endregion

        #region Constructor

        public DashboardViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IWeatherService weatherService):base(navigationService,pageDialogService)
        {
            this._weatherService = weatherService;
        }

        #endregion

        #region command

        public DelegateCommand RefreshDataCommand
        {
            get
            {
                return new DelegateCommand(async() =>
                {
                    IsRefreshing = true;
                    await GetGeoLocationAsync();
                    IsRefreshing = false;
                });
            }
        }

        #endregion

        #region Methods

        private async Task GetGeoLocationAsync()
        {
            try
            {
            
                var location = await Geolocation.GetLastKnownLocationAsync();

                var weatherRequest = new WeatherRequest
                {
                    lat = location.Latitude,
                    lon = location.Longitude
                };

                if(ValidateData(weatherRequest))
                {
                    var geoInfo = await ReturnWeatherInfoGeoAsync(weatherRequest);

                    if (geoInfo != null)
                    {
                        Temp = geoInfo.main.temp;

                        MaxTemp = geoInfo.main.temp_max;

                        MinTemp = geoInfo.main.temp_min;

                        var weather = geoInfo.weather?.Find(w => w != null);

                        Icon = weather?.icon;

                        Description = weather.description;

                        Humidity = geoInfo.main.humidity;

                        Speed = geoInfo.wind.speed;

                        LastRefreshed = DateTime.Now;

                        LocationName = geoInfo.name;
                    }
                }

            }
            catch (FeatureNotSupportedException fns)
            {
                await HandleExceptionAsync(fns, fns.Message);
            }
            catch(PermissionException pe)
            {
                await HandleExceptionAsync(pe, pe.Message);
            }
            catch(Exception ex)
            {
                await HandleExceptionAsync(ex);
            }
        }

        public async Task<WeatherObject> ReturnWeatherInfoGeoAsync(WeatherRequest weatherRequest)
        {
            try
            {
                return await _weatherService.GetWeatherInfoByGeoAsync(weatherRequest);
            }
            catch(Exception ex)
            {
                await HandleExceptionAsync(ex);

                return null;
            }
        }

        #endregion

        public override async void OnNavigatingTo(INavigationParameters parameters)
        {
            await GetGeoLocationAsync();
        }
       

    }
}
