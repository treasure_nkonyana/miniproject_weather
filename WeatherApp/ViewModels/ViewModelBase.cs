﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using WeatherApp.Constants;

namespace WeatherApp.ViewModels
{
    public class ViewModelBase : BindableBase,INavigatingAware
    {
        protected INavigationService NavigationService;
        protected IPageDialogService PageDialogService;

        public ViewModelBase(INavigationService navigationService, IPageDialogService pageDialogService)
        {
            this.NavigationService = navigationService;
            this.PageDialogService = pageDialogService;
        }

        public virtual void OnNavigatingTo(INavigationParameters parameters) { }

        protected async Task HandleExceptionAsync(Exception exception, string message = null)
        {
            
            if(exception !=null)
            {
                var httpServiceException = exception as WebException;

                HttpStatusCode? status = (httpServiceException.Response as HttpWebResponse)?.StatusCode;

                if(status == HttpStatusCode.InternalServerError)
                {
                    await PageDialogService.DisplayAlertAsync(StringConstants.DIALOG_ERROR_TITLE, "Server error occured", StringConstants.DIALOG_OK);
                }
            }
            else
            {
                if(string.IsNullOrWhiteSpace(message))
                    await PageDialogService.DisplayAlertAsync(StringConstants.DIALOG_ERROR_TITLE, message, StringConstants.DIALOG_OK);
                else
                    await PageDialogService.DisplayAlertAsync(StringConstants.DIALOG_ERROR_TITLE, "Something went wrong. Pull to refresh.", StringConstants.DIALOG_OK);
            }
        }

        protected virtual bool ValidateData(object input)
        {
            if(input != null)
            {
                var getObj = JsonConvert.SerializeObject(input);

                var setObj = JsonConvert.DeserializeObject<Dictionary<string, string>>(getObj);

                foreach(KeyValuePair<string,string> inputTemp in setObj)
                {
                    if (string.IsNullOrWhiteSpace(inputTemp.Value))
                        return false;
                }
                return true;
            }
            return false;
        }


    }
}
