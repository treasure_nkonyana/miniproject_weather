﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace WeatherApp.Converters
{
    public class TempConverter: IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var kelvin = (double)value;

            return (int)Math.Round(kelvin - 273.15,1);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
